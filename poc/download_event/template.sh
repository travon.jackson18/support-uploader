#!/bin/bash

function usage
{
    echo "usage: $0 <filename>"
    exit 1
}

function filesize
{
    local file=$1
    size=`stat -c%s $file 2>/dev/null` # linux
    if [ $? -eq 0 ]
    then
        echo $size
        return 0
    fi

    eval $(stat -s $file) # macos
    if [ $? -eq 0 ]
    then
        echo $st_size
        return 0
    fi

    return -1
}

if [[ -n $1 ]]; then
    file=$1
else
    usage
fi

if ! [[ -f $file ]]; then
    echo "Can't find file: $file !"
    usage
fi

if [[ $(filesize $file) -ge '%%MAX_UPLOAD_BYTES%%' ]]; then
    echo 'The filesize limit is currently %%MAX_UPLOAD_BYTES_PRETTY%%!'
    usage
fi

res=$(type curl 2>&1)
if [[ $? -ne 0 ]]; then
    echo "Error: Unable to find curl command (required for uploading)."
    exit 1
fi

filename=$(basename "$file")
curl_cmd=$(cat <<EOCURL
curl -H Content-Type=multipart/form-data %%HIDDENINPUT%% -F file=@"${file}" %%URL%%
EOCURL
        )
res=`eval $curl_cmd`

if [[ $? -eq 0 && $res == "" ]]; then
    echo "File $file uploaded successfully."
    exit 0
else
    echo "Error: $file failed to upload!"
    echo "$res"
    exit 1
fi
