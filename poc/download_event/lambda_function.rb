# frozen_string_literal: true

require 'aws-sdk-s3'
require 'aws-sdk-secretsmanager'
require 'base64'
require 'filesize'
require 'json'
require 'zip'

HTML_FILENAME = 'gs_upload.html'
BASH_FILENAME = 'gs_upload.sh'
HTML_TEMPLATE = File.read('template.html')
BASH_TEMPLATE = File.read('template.sh')

##
# This lambda function generates a html file and a bash script and returns
# a zip file containing both. The html file and bash script can be used to
# upload a file to the Support Uploader bucket.

def lambda_handler(event:, context:)
  ticket_id = event.dig('queryStringParameters', 'ticket')
  p ticket_id

  if ticket_id.nil? || !ticket_id.match(/\A\d+\z/)
    p 'statusCode: 400'
    return {
      statusCode: 400,
      body: JSON.generate("Undefined ticket id received: #{ticket_id}")
    }
  end

  presigned_data = generate_presigned_data(ticket_id: ticket_id)
  generate_and_respond_with_attachment(presigned_data: presigned_data, ticket_id: ticket_id)
end

def max_upload_bytes
  ENV['MAX_UPLOAD_BYTES'].to_i
end

def parse_secret
  secret_name = ENV['SECRET_NAME']
  region_name = ENV['AWS_REGION']

  client = Aws::SecretsManager::Client.new(region: region_name)

  get_secret_value_response = client.get_secret_value(secret_id: secret_name)
  secret_string = get_secret_value_response.secret_string

  JSON.parse secret_string
end

def generate_presigned_data(ticket_id:)
  # actually the region where lambda runs, for the moment
  # assuming it's the same, otherwise this needs updating
  region = ENV['AWS_REGION']
  bucket = ENV['BUCKET_NAME']
  expiration = Time.now + ENV['EXPIRATION_IN_HOURS'].to_i * 60 * 60
  p "Expires at: #{expiration}"

  secret = parse_secret
  client = Aws::S3::Client.new(
    access_key_id: secret['ACCESS_KEY_ID'],
    secret_access_key: secret['ACCESS_SECRET_KEY']
  )
  s3_bucket = Aws::S3::Resource.new(client: client, region: region).bucket(bucket)

  presigned_data = s3_bucket.presigned_post(
    key: 'upload/' + ticket_id + "/#{Time.now.to_i.to_s}-${filename}",
    signature_expiration: expiration,
    content_length_range: 0..max_upload_bytes
  )

  presigned_data
end

def encoded_zip_buffer(html:, bash:)
  zip_buffer = Zip::OutputStream.write_buffer do |zio|
    zio.put_next_entry(HTML_FILENAME)
    zio.write html
    zio.put_next_entry(BASH_FILENAME)
    zio.write bash
  end

  Base64.encode64(zip_buffer.string)
end

def generate_and_respond_with_attachment(presigned_data:, ticket_id:)
  zip_filename = "gs_uploader_#{ticket_id}.zip"
  html_text = html_data(presigned_data.fields, presigned_data.url)
  bash_text = bash_data(presigned_data.fields, presigned_data.url)

  {
    statusCode: 200,
    headers: {
      'Content-Type': 'application/zip',
      'Content-Disposition': "attachment; filename=#{zip_filename}"
    },
    isBase64Encoded: true,
    body: encoded_zip_buffer(html: html_text, bash: bash_text)
  }
end

def replace_custom_template_fields(template)
  template = template.gsub('%%MAX_UPLOAD_BYTES%%', max_upload_bytes.to_s)
  template = template.gsub('%%MAX_UPLOAD_BYTES_PRETTY%%', Filesize.new("#{max_upload_bytes} B").pretty)

  template
end

##
# Returns a html file in a string containing a form to upload a file to
# The support uploader.

def html_data(hidden_fields, url)
  html_templ = HTML_TEMPLATE
  form_input = ''

  hidden_fields.each do |name, value|
    form_input += "<input type='hidden' name='#{name}' value='#{value}' />\n"
  end

  html_templ = html_templ.gsub('%%URL%%', url)
  html_templ = html_templ.gsub('%%HIDDENINPUT%%', form_input)
  html_templ = replace_custom_template_fields(html_templ)

  html_templ
end

##
# Returns a bash script in a string which can be used to upload a file to
# The support uploader.

def bash_data(hidden_fields, url)
  bash_templ = BASH_TEMPLATE
  form_input = ''

  hidden_fields.each do |name, value|
    form_input += ' -F "' + name + '=' + value + "\"\n"
  end

  bash_templ = bash_templ.gsub('%%HIDDENINPUT%%', form_input)
  bash_templ = bash_templ.gsub('%%URL%%', url)
  bash_templ = replace_custom_template_fields(bash_templ)

  bash_templ
end
