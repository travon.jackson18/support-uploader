# frozen_string_literal: true

require 'json'
require 'zendesk_api'
require 'aws-sdk-secretsmanager'

def parse_secret
  secret_name = ENV['SECRET_NAME']
  region_name = ENV['AWS_REGION']

  client = Aws::SecretsManager::Client.new(region: region_name)

  get_secret_value_response = client.get_secret_value(secret_id: secret_name)
  secret_string = get_secret_value_response.secret_string

  JSON.parse secret_string
end

# Creates an ZD API instance singleton
class ZendeskApiClient < ZendeskAPI::Client
  def self.instance
    @instance ||= new do |config|
      secret = parse_secret
      config.url = secret['ZD_URL']
      config.username = secret['ZD_USERNAME']
      config.password = secret['ZD_TOKEN']
      config.retry = true
    end
  end
end

def zd_client
  ZendeskApiClient.instance
end

def notice_ticket_of_uploads(ticket_id:, filename:)
  escaped_file = filename.gsub("+", "%20")
  file_url = "https://s3.console.aws.amazon.com/s3/object/gitlab-support-uploader/upload/#{ticket_id}/#{escaped_file}"

  notice_msg = <<~BODY
    New files uploaded by the customer, please check the S3 bucket:

    * [#{filename}](#{file_url})
  BODY

  ticket = zd_client.tickets.find!(id: ticket_id)
  ticket.comment = { body: notice_msg, public: false }
  ticket.save!
end

def extract_ticket_and_file_from_event(event:)
  record = event['Records'].find do |r|
    r['eventSource'] == 'aws:s3' &&
      r['s3']['bucket']['name'] == ENV['BUCKET_NAME']
  end

  object_key = record['s3']['object']['key']

  object_key.scan(%r{upload/(\d+)/(.*)}).to_a.flatten
end

def lambda_handler(event:, context:)
  ticket_id, filename = extract_ticket_and_file_from_event(event: event)

  if ticket_id.nil?
    error = "Unable to extract ticket from #{event['eventSource']}"

    puts error
    return { statusCode: 500, body: JSON.generate(error) }
  end

  notice_ticket_of_uploads(ticket_id: ticket_id, filename: filename)

  { statusCode: 200, body: JSON.generate("Updated the ticket ##{ticket_id}") }
end
