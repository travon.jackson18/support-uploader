# Proof of Concept script - gen.py

## gen.py

gen.py is a proof of concept script which internally creates a presigned url and then creates 2 files which a customer would use to upload a file to Gitlab Support.

## Usage

./gen.py -t *ticket_number*

This produces 2 files:  
**gitlab_support.html** - html file which contains a simple form, used to upload a file to the Gitlab Support Bucket.  
**sup_upload.sh** - bash script which a customer can use to upload a file to the Gitlab Support Bucket.

## Test Bucket

The script currently uses the S3 bucket `roconnor-test-bucket1` in the [Gitlab Support AWS account](https://s3.console.aws.amazon.com/s3/buckets/roconnor-test-bucket1). 

Files are uploaded to s3://roconnor-test-bucket1/upload/*ticket_number*
